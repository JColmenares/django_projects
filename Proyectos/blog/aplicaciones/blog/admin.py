from django.contrib import admin
from .models import *
from import_export import resources         #Necesario para poder importar t exportar
from import_export.admin import ImportExportModelAdmin #Necesario para poder importar t exportar


class CategoriaResource(resources.ModelResource):
    class Meta:
        model = Categoria

class CategoriaAdmin(ImportExportModelAdmin,admin.ModelAdmin): #Permite agregarle extras al menú de administrador de datos
    search_fields = ['nombre']          #Permite agregar barra de busqueda y agregar como se quiere filtrar
    list_display = ('nombre','estado','fecha_creacion') #Sirve especificar la información que se mostrara en los registros de los modelos
    resources_class = CategoriaResource

class AutorResource(resources.ModelResource):
    class Meta:
        model = Autor

class AutorAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    search_fields = ['nombres','apellidos','correo']
    list_display = ('nombres','apellidos','correo','estado','fecha_creacion')
    resources_class = AutorResource

admin.site.register(Categoria,CategoriaAdmin)
admin.site.register(Autor,AutorAdmin)
admin.site.register(Post)
