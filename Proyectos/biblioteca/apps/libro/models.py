from django.db import models

# Create your models here.
class Autor(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 200, blank = False, null = False)
    apellidos = models.CharField(max_length = 220, blank = False, null = False)
    nacionalidad = models.CharField(max_length = 100, blank = False, null = False)
    descripcion = models.TextField(blank = False, null = False)
    estado = models.BooleanField("Estado", default= True)
    fecha_creacion = models.DateField('Fecha de creación', auto_now = True, auto_now_add = False)

    class Meta: #metas datos, datos extras para el modelo - modo visual en admin django
        verbose_name = 'Autor' #Permite asignarle un nombre en singular para el titulo del registro
        verbose_name_plural = 'Autores' #Permite asignarle un nombre en pural para el titulo del registro
        ordering = ['nombre']  #Permite ordenar los regisros 

    def __str__(self): #Permite identificar como se va a llamar cada registro guardado en el modelo
        return self.nombre + ' ' + self.apellidos
        
class Libro(models.Model):
    id = models.AutoField(primary_key = True)
    titulo = models.CharField('Titulo', max_length = 255, blank = False, null = False)
    fecha_publicacion = models.DateField('Fecha de publicación', blank = False, null = False)
    autor_id = models.ManyToManyField(Autor)
                    # models.ManyToManyField(Nombre_del_Modelo) Sirve para relacionar varios autor con un libro o varios libros (Relacion muchos a muchos)
                    # OneToOneField(Nombre_del_Modelo ,on_delete = models.CASCADE) # Sirve para relacionar un solo libro con un autor (Relacion uno a uno)
                    # ForeignKey(Nombre_del_Modelo,on_delete = models.CASCADE) #  Sirve para relacionar a un autor con varios libros (Relacion uno a muchos)
                    # El atributo on_delete = CASCADE  sirve para que se elimine el regitro que esta relacionado directamente con el cuando fue creado
    fecha_creacion = models.DateField('Fecha de creación', auto_now = True, auto_now_add = False)
    
    
    class Meta: #metas datos, datos extras para el modelo - modo visual en admin django
        verbose_name = 'Libro' #Permite asignarle un nombre en singular para el titulo del registro
        verbose_name_plural = 'Libros' #Permite asignarle un nombre en pural para el titulo del registro
        ordering = ['titulo']  #Permite ordenar los regisros 

    def __str__(self): #Permite identificar como se va a llamar cada registro guardado en el modelo
        return self.titulo