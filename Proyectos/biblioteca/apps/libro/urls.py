from django.urls import path,re_path
#from .views import Home # se importa la función que llamara la vista
from .views import crearAutor,listarAutor,editarAutor,eliminarAutor

urlpatterns = [
    #path('',Home,name = 'index') # ('texto de bara de direcciones',función Home ubicado en la views, identidicador de la url)
    path('crear_autor/',crearAutor,name = 'crear_autor'),
    path('listar_autor/',listarAutor, name = 'listar_autor'),
    path('editar_autor/<int:id>',editarAutor, name = 'editar_autor'),
    path('eliminar_autor/<int:id>',eliminarAutor, name = 'eliminar_autor')






    #Se puede pasar dato por la url ejemplo 
    # ('listar_autor/<slug:titulo>/int:pk')
    #en versiones anteriroes se usaba la función url() para pasar expresiones regulares
    
    
    #La función re_path permite usar las expresiones regulares
    #ejemplo 
    #re_path(r'^crear_autor/(?P<id>\d+)',crearAutor,name="csskf")

    #NOTA: Las expresiones regulares solo se pueden usar en la función re_path()
]
